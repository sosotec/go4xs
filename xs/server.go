package xs

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
)

func (connection *Connection) Exec(command *Command) (*Command, error) {

	b, _ := json.Marshal(command)
	fmt.Println(string(b))

	if (command.Cmd & 0x80) > 0 {
		return new(Command), nil
	}

	buf := command.Encode(connection.IsBigEndian)
	//for _, b := range buf {
	//	//fmt.Printf("%02x - %v\n", b, b)
	//	fmt.Printf("%v\n", b)
	//}

	_, err := connection.conn.Write(buf)
	if err != nil {
		logRecord("%s [%v] Exec Err: %s", connection.addr, command.Cmd, err.Error())
		return nil, err
	}

	res, err := connection.GetResponse()
	if err != nil {
		logRecord("%s [%v] Exec GetResponse Err: %s", connection.addr, command.Cmd, err.Error())
		return nil, err
	}
	if res.Cmd == CMD_ERR {
		logRecord("%s [%v] Exec RES CMD Err", connection.addr, command.Cmd)
		return nil, errors.New(res.Buf)
	}

	return res, nil
}

func (connection *Connection) ExecBatch(commands []*Command) (*Command, error) {

	buffers := bytes.NewBuffer([]byte{})
	for _, command := range commands {

		b, _ := json.Marshal(command)
		fmt.Println(string(b))

		buf := command.Encode(connection.IsBigEndian)
		buffers.Write(buf)
	}

	data := buffers.Bytes()
	buffers.Reset()

	_, err := connection.conn.Write(data)
	if err != nil {
		logRecord("%s ExecBatch Err: %s", connection.addr, err.Error())
		return nil, err
	}

	res, err := connection.GetResponse()
	if err != nil {
		logRecord("%s ExecBatch GetResponse Err: %s", connection.addr, err.Error())
		return nil, err
	}
	if res.Cmd == CMD_ERR {
		logRecord("%s ExecBatch RES CMD Err", connection.addr)
		return nil, errors.New(res.Buf)
	}

	return res, nil
}

func (connection *Connection) SetProjectName(name string) error {
	cmd := NewCommand(CMD_USE, 0, 0, name)
	if _, err := connection.Exec(cmd); err != nil {
		logRecord("%s SetProjectName Err: %s", connection.addr, err.Error())
		return err
	}
	return nil
}

func (connection *Connection) GetResponse() (*Command, error) {

	reader := connection.reader
	head := make([]byte, 8)
	count, err := reader.Read(head)
	if err != nil {
		logRecord("%s GetResponse Read Err: %s", connection.addr, err.Error())
		return nil, err
	}
	if count != 8 {
		return nil, fmt.Errorf("no more data")
	}

	command := &Command{}
	command.Cmd = head[0]
	command.Arg1 = head[1]
	command.Arg2 = head[2]

	var len2 = head[3]
	var len1 = uint32(head[4])
	if connection.IsBigEndian {
		len1 = len1 << 24
		len1 = len1 | uint32(head[5])<<16
		len1 = len1 | uint32(head[6])<<8
		len1 = len1 | uint32(head[7])
	} else {
		len1 = len1 | uint32(head[5])<<8
		len1 = len1 | uint32(head[6])<<16
		len1 = len1 | uint32(head[7])<<24
	}

	//fmt.Println(len1, len2)

	if len2 > 0 {
		buf := make([]byte, len2)
		count, err = reader.Read(buf)
		if err != nil || uint8(count) != len2 {
			return nil, fmt.Errorf("read data 2 error")
		}
		command.Buf1 = string(buf)
	}

	if len1 > 0 {
		buf := make([]byte, len1)
		count, err = reader.Read(buf)
		if err != nil || uint32(count) != len1 {
			return nil, fmt.Errorf("read data 1 error")
		}
		command.Buf = string(buf)
	}

	//返回结果
	return command, nil
}
