package xs

import "github.com/robfig/config"

type Setting struct {
	ProjectName           string
	ProjectDefaultCharset string
	IndexServer           string
	SearchServer          string
	Schema                *Schema
}

func NewSetting(ini string) *Setting {

	file, _ := config.ReadDefault(ini)

	setting := &Setting{}
	schema := &Schema{}
	setting.Schema = schema
	schema.Fields = make(map[uint8]*Field)

	for idx, sec := range file.Sections() {
		if sec == "DEFAULT" {
			setting.ProjectName, _ = file.String(sec, "project.name")
			setting.ProjectDefaultCharset, _ = file.String(sec, "project.default_charset")
			setting.IndexServer, _ = file.String(sec, "server.index")
			setting.SearchServer, _ = file.String(sec, "server.search")
			continue
		}

		field := &Field{}
		field.Name = sec
		field.Weight = 1

		opts, _ := file.Options(sec)
		for _, opt := range opts {
			switch opt {
			case "index":
				field.Index, _ = file.String(sec, opt)
			case "type":
				field.Type, _ = file.String(sec, opt)
			case "tokenizer":
				field.Tokenizer, _ = file.String(sec, opt)
			case "cutlen":
			//field.CutLen, _ = c.Int(sec, opt)
			case "weight":
				//field.Weight, _ = c.Int(sec, opt)
			}
		}

		if field.Type == "body" {
			field.Vno = MIXED_VNO
			field.CutLen = 300
			field.Flag = FLAG_INDEX_SELF | FLAG_WITH_POSITION
			schema.Body = field
		} else {
			if field.Type == "id" {
				field.Flag = FLAG_INDEX_SELF
				schema.Id = field
			} else if field.Type == "title" {
				field.Weight = 5
				field.Flag = FLAG_INDEX_BOTH | FLAG_WITH_POSITION
				schema.Title = field
			}
			field.Vno = uint8(idx - 1)
		}

		schema.Fields[field.Vno] = field
	}

	if setting.ProjectDefaultCharset == "" {
		setting.ProjectDefaultCharset = "UTF-8"
	}

	return setting
}
