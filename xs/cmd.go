package xs

type Command struct {
	Cmd  uint8
	Arg1 uint8  //其值为 0~255, 具体含义视不同 CMD 而确定
	Arg2 uint8  //其值为 0~255, 具体含义视不同 CMD 而确定, 常用于存储 value no
	Buf  string //字符串内容, 最大长度为 2GB
	Buf1 string //字符串内容1, 最大长度为 255字节
}

func NewCommand(cmd uint8, arg1 uint8, arg2 uint8, buf ...string) *Command {
	command := new(Command)
	command.Cmd = cmd
	command.Arg1 = arg1
	command.Arg2 = arg2
	if len(buf) >= 1 {
		command.Buf = buf[0]
		if len(buf) >= 2 {
			command.Buf1 = buf[1]
		}
	}
	return command
}

// GetArg return the the
func (command *Command) GetArg() uint16 {
	var arg uint16
	arg = uint16(command.Arg1)
	arg = (arg << 8) | uint16(command.Arg2)
	return arg
}

// SetArg sets the arg1 and arg2 by on value
func (command *Command) SetArg(arg uint16) {
	command.Arg1 = uint8(arg >> 8)
	command.Arg2 = uint8(arg & 0xff)
}
