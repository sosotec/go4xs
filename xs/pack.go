package xs

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// Pack date into String
func Pack(format string, args ...interface{}) (string, error) {
	if len(format) != len(args) {
		return "", fmt.Errorf("format length %d != args length %d", len(format), len(args))
	}
	buf := bytes.NewBuffer([]byte{})
	for i := 0; i < len(format); i++ {
		t := fmt.Sprintf("%T", args[i])
		if m, _ := regexp.Match("^u?int(8|16|32|64)?$", []byte(t)); !m {
			return "", fmt.Errorf("%v type is %T, it's an invalid type for Pakc", args[i], args[i])
		}
		switch format[i] {
		case 'I':
			_ = binary.Write(buf, binary.LittleEndian, args[i].(uint32))
			break
		case 'C':
			_ = buf.WriteByte(args[i].(uint8))
			break
		case 'n':
			_ = binary.Write(buf, binary.BigEndian, args[i].(uint16))
			break
		default:
		}
	}
	return string(buf.Bytes()), nil
}

func UnPack(format, data string) (map[string]interface{}, error) {
	parts := strings.Split(format, "/")
	unpacked := make(map[string]interface{})
	buf := bytes.NewBufferString(data)

	for idx, part := range parts {
		na := strconv.Itoa(idx)
		if len(part) > 1 {
			na = part[1:]
		}
		switch part[0] {
		case 'I':
			var val uint32
			if err := binary.Read(buf, binary.LittleEndian, &val); err != nil {
				return unpacked, err
			}
			unpacked[na] = val
			break
		case 'C':
			var val uint8
			if err := binary.Read(buf, binary.LittleEndian, &val); err != nil {
				return unpacked, err
			}
			unpacked[na] = val
			break
		case 'i':
			var val int32
			if err := binary.Read(buf, binary.LittleEndian, &val); err != nil {
				return unpacked, err
			}
			unpacked[na] = val
			break
		case 'f':
			var val float32
			if err := binary.Read(buf, binary.LittleEndian, &val); err != nil {
				return unpacked, err
			}
			unpacked[na] = val
			break
		default:
			unpacked[na] = nil
		}
	}
	return unpacked, nil
}
