package xs

import (
	"fmt"
	"log"
	"os"
	"time"
)

var (
	currentDate string
	logHandler  *log.Logger
)

//初始化
func getHandler() *log.Logger {

	//当前日期
	date := time.Now().Format("2006-01-02")
	if currentDate == date && logHandler != nil {
		return logHandler
	}
	currentDate = date

	//打开Info Logger文件
	f, err := openFile("xs-"+currentDate+".log", "runtime/log/")
	if err != nil {
		panic(err)
	}

	//自定义输出日志
	logHandler = log.New(f, "[xs] ", log.Ldate|log.Lmicroseconds)

	//返回
	return logHandler
}

func logRecord(s string, v ...interface{}) (text string) {
	handler := getHandler()
	if len(v) == 0 {
		text = s
	} else {
		text = fmt.Sprintf(s, v...)
	}
	_ = handler.Output(3, text)
	fmt.Println(text)
	return
}

// openFile 找开文件句柄
func openFile(fileName, filePath string) (*os.File, error) {

	//获取当前上去
	dir, _ := os.Getwd()

	//检查日志文件夹
	src := dir + "/" + filePath

	//如果文件夹不存在
	if err := isNotExistAndMkDir(src); err != nil {
		return nil, fmt.Errorf("mkdir %s, err: %v", src, err)
	}

	//检查文件夹是否有权限
	if checkPermission(src) == true {
		return nil, fmt.Errorf("permission denied: %s", src)
	}

	//打开文件
	if f, err := open(src+fileName, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644); err != nil {
		return nil, fmt.Errorf("fail to open file :%v", err)
	} else {
		return f, nil
	}
}

func checkNotExist(src string) bool {
	_, err := os.Stat(src)
	return os.IsNotExist(err)
}

func checkPermission(src string) bool {
	_, err := os.Stat(src)
	return os.IsPermission(err)
}

func isNotExistAndMkDir(src string) error {
	if notExist := checkNotExist(src); notExist == true {
		if err := os.MkdirAll(src, 755); err != nil {
			return err
		}
	}
	return nil
}

func open(name string, flag int, perm os.FileMode) (*os.File, error) {
	f, err := os.OpenFile(name, flag, perm)
	if err != nil {
		return nil, err
	}
	return f, nil
}
