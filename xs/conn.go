package xs

import (
	"bufio"
	"net"
	"time"
)

// Connection to indexer or searcher server
type Connection struct {
	conn        *net.TCPConn
	addr        *net.TCPAddr
	reader      *bufio.Reader
	createTime  time.Time
	IsBigEndian bool
}

// NewConnection to server
func NewConnection(addr string) (*Connection, error) {

	var connection *Connection
	tcpAddr, err := net.ResolveTCPAddr("tcp", addr)
	if err != nil {
		return nil, err
	}
	connection = &Connection{}
	connection.addr = tcpAddr

	conn, err := net.DialTCP("tcp", nil, connection.addr)
	if err != nil {
		return nil, err
	}

	_ = conn.SetReadBuffer(10240)
	_ = conn.SetKeepAlive(true)
	_ = conn.SetKeepAlivePeriod(time.Second * 5)

	connection.conn = conn
	connection.reader = bufio.NewReader(connection.conn)
	connection.createTime = time.Now()

	return connection, nil
}

// Close the connection
func (connection *Connection) Close() {
	if connection.conn != nil {
		_ = connection.conn.Close()
		connection.conn = nil
		connection.reader = nil
	}
}
