package xs

import (
	"bytes"
	"encoding/binary"
)

func (command *Command) Encode(bigEndian bool) []byte {

	lenOfBuf1 := len(command.Buf1)
	if lenOfBuf1 > 0xff {
		command.Buf1 = command.Buf1[0:0xff]
		lenOfBuf1 = 0xff
	}
	lenOfBuf := len(command.Buf)

	buf := make([]byte, 8+lenOfBuf+lenOfBuf1)
	buf[0] = command.Cmd      //1
	buf[1] = command.Arg1     //1
	buf[2] = command.Arg2     //1
	buf[3] = uint8(lenOfBuf1) //1

	bInt := uint32(lenOfBuf)
	bytesBuff := bytes.NewBuffer([]byte{})

	if bigEndian {
		_ = binary.Write(bytesBuff, binary.BigEndian, bInt)
	} else {
		_ = binary.Write(bytesBuff, binary.LittleEndian, bInt)
	}
	copy(buf[4:], bytesBuff.Bytes()) //4
	idx := 8
	if lenOfBuf > 0 {
		copy(buf[idx:], command.Buf)
		idx += lenOfBuf
	}
	if lenOfBuf1 > 0 {
		copy(buf[idx:], command.Buf1)
	}
	return buf
}
