package xs

// DocResSize is the len of the doc buf
const (
	DocResSize   int    = 20
	DocResFormat string = "IdocId/Irank/Icount/ipercent/fweight"
)

// Document for search result
type Document struct {
	Fields  map[string]string `json:"fields"`
	DocId   uint32            `json:"docId"`
	Rank    uint32            `json:"rank"`
	Count   uint32            `json:"count"`
	Percent int32             `json:"percent"`
	Weight  float32           `json:"weight"`
	Matched []string          `json:"matched"`
}

// NewDocument creates document instance from meta
func NewDocument(meta string) (*Document, error) {

	res, err := UnPack(DocResFormat, meta)
	if err != nil {
		return nil, err
	}

	doc := &Document{}
	doc.Fields = make(map[string]string)
	doc.DocId = res["docId"].(uint32)    //4
	doc.Rank = res["rank"].(uint32)      //4
	doc.Count = res["count"].(uint32)    //4
	doc.Percent = res["percent"].(int32) //4
	doc.Weight = res["weight"].(float32) //4

	return doc, nil
}
