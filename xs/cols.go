package xs

const (
	MAX_WDF            = 0x3f
	MIXED_VNO          = 255
	TYPE_STRING        = 0
	TYPE_NUMERIC       = 1
	TYPE_DATE          = 2
	TYPE_ID            = 10
	TYPE_TITLE         = 11
	TYPE_BODY          = 12
	FLAG_INDEX_SELF    = 0x01
	FLAG_INDEX_MIXED   = 0x02
	FLAG_INDEX_BOTH    = 0x03
	FLAG_WITH_POSITION = 0x10
	FLAG_NON_BOOL      = 0x80
)

type Field struct {
	Vno       uint8
	Name      string
	Type      string
	Index     string
	Weight    uint16
	CutLen    uint16
	Flag      int
	Tokenizer string
}

// Schema of Document to be indexed
type Schema struct {
	Id     *Field
	Title  *Field
	Body   *Field
	Fields map[uint8]*Field
}

func (schema *Schema) GetFieldDict() map[string]*Field {
	var dict = make(map[string]*Field)
	for _, field := range schema.Fields {
		dict[field.Name] = field
	}
	return dict
}

func (field *Field) WithPos() bool {
	return (field.Flag & FLAG_WITH_POSITION) > 0
}

func (field *Field) IsBoolIndex() bool {
	if (field.Flag & FLAG_NON_BOOL) > 0 {
		return false
	}
	return field.HasIndex()
}

func (field *Field) IsNumeric() bool {
	return field.Type == "numeric"
}

func (field *Field) IsSpecial() bool {
	return field.Type == "id" || field.Type == "title" || field.Type == "body"
}

func (field *Field) HasIndexSelf() bool {
	return (field.Flag & FLAG_INDEX_SELF) > 0
}

func (field *Field) HasIndexMixed() bool {
	return (field.Flag & FLAG_INDEX_MIXED) > 0
}

func (field *Field) HasIndex() bool {
	return (field.Flag & FLAG_INDEX_BOTH) > 0
}
