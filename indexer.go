package go4xs

import (
	"bytes"
	"errors"
	"fmt"
	"gitee.com/sosotec/go4xs/xs"
	"strconv"
	"strings"
)

type Indexer struct {
	setting    *xs.Setting
	connection *xs.Connection
}

func (i *Indexer) LoadConf(setting *xs.Setting) {
	i.setting = setting
}

func (i *Indexer) Close() {
	i.connection.Close()
}

func (i *Indexer) Connect() error {

	//获取服务器
	if conn, err := xs.NewConnection(i.setting.IndexServer); err != nil {
		return err
	} else {
		i.connection = conn
	}

	//设置DB
	if err := i.connection.SetProjectName(i.setting.ProjectName); err != nil {
		return err
	}

	return nil
}

func (i *Indexer) Add(values ...map[string]string) error {
	return i.save(true, values...)
}

func (i *Indexer) Update(value map[string]string) error {
	return i.save(false, value)
}

func (i *Indexer) save(insert bool, values ...map[string]string) error {

	var cmds []*xs.Command
	var conn = i.connection
	var setting = i.setting

	//起始命令
	if insert {
		cmds = append(cmds, xs.NewCommand(xs.CMD_INDEX_REQUEST, xs.CMD_INDEX_REQUEST_ADD, 0))
	} else {
		if len(values) != 1 {
			return errors.New("only one record support in update")
		}
		var key = setting.Schema.Id.Vno
		var value = values[0][setting.Schema.Id.Name]
		cmds = append(cmds, xs.NewCommand(xs.CMD_INDEX_REQUEST, xs.CMD_INDEX_REQUEST_UPDATE, key, value))
	}

	//内容命令
	var fields = setting.Schema.GetFieldDict()

	//循化提交内容
	for _, value := range values {

		//每份内容
		for name, content := range value {

			if _, ok := fields[name]; !ok {
				continue
			}

			var field = fields[name]
			var arg1 = uint8(0)
			if field.IsNumeric() {
				arg1 = xs.CMD_VALUE_FLAG_NUMERIC
			}

			//term
			if field.Type == "id" {
				cmds = append(cmds, xs.NewCommand(xs.CMD_DOC_TERM, 1, field.Vno, content))
			}

			//title
			if field.Type == "title" {
				wdf := uint8(field.Weight | xs.CMD_INDEX_FLAG_WITHPOS)
				cmds = append(cmds, xs.NewCommand(xs.CMD_DOC_INDEX, wdf, xs.MIXED_VNO, content))

				wdf |= uint8(field.Weight | xs.CMD_INDEX_FLAG_SAVEVALUE)
				cmds = append(cmds, xs.NewCommand(xs.CMD_DOC_INDEX, wdf, field.Vno, content))

				continue
			}

			//body
			if field.Type == "body" {
				wdf := uint8(field.Weight | xs.CMD_INDEX_FLAG_WITHPOS | xs.CMD_INDEX_FLAG_SAVEVALUE)
				cmds = append(cmds, xs.NewCommand(xs.CMD_DOC_INDEX, wdf, xs.MIXED_VNO, content))
				continue
			}

			//value
			cmds = append(cmds, xs.NewCommand(xs.CMD_DOC_VALUE, arg1, field.Vno, content))
		}

		//叠加提交命令（结束）
		cmds = append(cmds, xs.NewCommand(xs.CMD_INDEX_SUBMIT, 0, 0))

		//执行命令
		if _, err := conn.ExecBatch(cmds); err != nil {
			return err
		}
	}

	//Commit命令
	//if _, err := conn.Exec(xs.NewCommand(xs.CMD_INDEX_COMMIT, 0, 0)); err != nil {
	//	return err
	//}

	//返回
	return nil
}

func (i *Indexer) Delete(ids ...string) error {

	var cmds []*xs.Command
	var conn = i.connection

	//主键列
	var field = i.setting.Schema.Id

	//循化叠加命令
	for _, id := range ids {
		if id == "" {
			continue
		}
		cmds = append(cmds, xs.NewCommand(xs.CMD_INDEX_REMOVE, 0, field.Vno, strings.ToLower(id)))
	}

	//提交命令
	if len(cmds) == 1 {
		if _, err := conn.Exec(cmds[0]); err != nil {
			return err
		}
	} else {
		buffer := bytes.NewBuffer([]byte{})
		for _, cmd := range cmds {
			buf := cmd.Encode(conn.IsBigEndian)
			buffer.Write(buf[:])
		}
		buf := string(buffer.Bytes())
		if _, err := conn.Exec(xs.NewCommand(xs.CMD_INDEX_EXDATA, 0, 0, buf)); err != nil {
			return err
		}
	}

	//返回
	return nil
}

func (i *Indexer) Clean() error {
	var conn = i.connection
	if _, err := conn.Exec(xs.NewCommand(xs.CMD_INDEX_CLEAN_DB, 0, 0)); err != nil {
		return err
	}
	return nil
}

func (i *Indexer) BeginRebuild() error {
	var conn = i.connection
	if _, err := conn.Exec(xs.NewCommand(xs.CMD_INDEX_REBUILD, 0, 0)); err != nil {
		return err
	}
	return nil
}

func (i *Indexer) EndRebuild() error {
	var conn = i.connection
	if _, err := conn.Exec(xs.NewCommand(xs.CMD_INDEX_REBUILD, 1, 0)); err != nil {
		return err
	}
	return nil
}

func (i *Indexer) StopRebuild() error {
	var conn = i.connection
	if _, err := conn.Exec(xs.NewCommand(xs.CMD_INDEX_REBUILD, 2, 0)); err != nil {
		return err
	}
	return nil
}

func (i *Indexer) AddSynonym(word string, synonym string) error {
	if word == "" || synonym == "" {
		return nil
	}
	var conn = i.connection
	if _, err := conn.Exec(xs.NewCommand(xs.CMD_INDEX_SYNONYMS, xs.CMD_INDEX_SYNONYMS_ADD, 0, word, synonym)); err != nil {
		return err
	}
	return nil
}

func (i *Indexer) DelSynonym(word string, synonym string) error {
	if word == "" {
		return nil
	}
	var conn = i.connection
	if _, err := conn.Exec(xs.NewCommand(xs.CMD_INDEX_SYNONYMS, xs.CMD_INDEX_SYNONYMS_DEL, 0, word, synonym)); err != nil {
		return err
	}
	return nil
}

func (i *Indexer) FlushLogging() error {
	var conn = i.connection
	if _, err := conn.Exec(xs.NewCommand(xs.CMD_FLUSH_LOGGING, 0, 0)); err != nil {
		return err
	}
	return nil
}

func (i *Indexer) FlushIndex() error {
	var conn = i.connection
	if _, err := conn.Exec(xs.NewCommand(xs.CMD_INDEX_COMMIT, 0, 0)); err != nil {
		return err
	}
	return nil
}

func (i *Indexer) GetScwsMulti() (int, error) {
	var conn = i.connection
	if res, err := conn.Exec(xs.NewCommand(xs.CMD_SEARCH_SCWS_GET, xs.CMD_SCWS_GET_MULTI, 0)); err != nil {
		return 0, err
	} else {
		fmt.Println(res)
		level, _ := strconv.Atoi(res.Buf)
		return level, nil
	}
}

func (i *Indexer) SetScwsMulti(level uint8) error {
	var conn = i.connection
	if level < 0 || level > 15 {
		return errors.New("level is out of range")
	}
	if _, err := conn.Exec(xs.NewCommand(xs.CMD_SEARCH_SCWS_SET, xs.CMD_SCWS_SET_MULTI, level)); err != nil {
		return err
	}
	return nil
}
