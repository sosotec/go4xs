package go4xs

import (
	"fmt"
	"gitee.com/sosotec/go4xs/xs"
	"math"
	"strings"
)

type Searcher struct {
	pool         *Pool
	setting      *xs.Setting
	connection   *xs.Connection
	queryPrefix  map[string]bool
	buffer       []*xs.Command
	defaultOp    uint8
	autoSynonyms int
}

type Result struct {
	Count     uint32         `json:"count"`
	Documents []*xs.Document `json:"documents"`
}

func (s *Searcher) LoadConf(setting *xs.Setting, pool *Pool) {
	s.setting = setting
	s.pool = pool
	s.defaultOp = xs.CMD_QUERY_OP_OR
}

func (s *Searcher) Close() {
	s.pool.Release(s.connection)
}

func (s *Searcher) Connect() error {

	//获取服务器
	if conn, err := s.pool.Acquire(); err != nil {
		return err
	} else {
		s.connection = conn
	}

	//设置DB
	if err := s.connection.SetProjectName(s.setting.ProjectName); err != nil {
		s.connection.Close()
		return err
	}

	//请空原来缓存命令
	s.clearBuffer()

	//初始化特殊字段
	for _, field := range s.setting.Schema.Fields {
		if field.CutLen > 0 {
			cln := uint8(math.Ceil(float64(field.CutLen) / 10.0))
			if cln > 127 {
				cln = 127
			}
			cmd := xs.NewCommand(xs.CMD_SEARCH_SET_CUT, cln, field.Vno)
			s.buffer = append(s.buffer, cmd)
		}
		if field.IsNumeric() {
			cmd := xs.NewCommand(xs.CMD_SEARCH_SET_NUMERIC, 0, field.Vno)
			s.buffer = append(s.buffer, cmd)
		}
	}

	return nil
}

func (s *Searcher) SetFuzzy(fuzzy bool) {
	if fuzzy {
		s.defaultOp = xs.CMD_QUERY_OP_OR
	} else {
		s.defaultOp = xs.CMD_QUERY_OP_AND
	}
}

func (s *Searcher) SetAutoSynonyms() {
	s.autoSynonyms = xs.CMD_PARSE_FLAG_BOOLEAN | xs.CMD_PARSE_FLAG_PHRASE | xs.CMD_PARSE_FLAG_LOVEHATE | xs.CMD_PARSE_FLAG_AUTO_MULTIWORD_SYNONYMS
}

func (s *Searcher) Query(keyword string, args ...uint32) (*Result, error) {

	var size uint32 = 10
	if len(args) >= 1 {
		size = args[0]
	}
	if size <= 0 {
		size = 10
	}
	var offset uint32 = 0
	if len(args) >= 2 {
		offset = (args[1] - 1) * size
	}

	if s.autoSynonyms > 0 {
		cmd := xs.NewCommand(xs.CMD_QUERY_PARSEFLAG, 0, 0)
		cmd.SetArg(uint16(s.autoSynonyms))
		s.buffer = append(s.buffer, cmd)
	}

	if pos := strings.Index(keyword, ":"); pos > 0 {
		part1 := keyword[:pos]
		fields := s.setting.Schema.GetFieldDict()
		if s.queryPrefix == nil {
			s.queryPrefix = map[string]bool{}
		}
		if field, ok := fields[part1]; ok && field.Vno != xs.MIXED_VNO {
			if _, exit := s.queryPrefix[part1]; !exit {
				arg1 := xs.CMD_PREFIX_NORMAL
				if field.IsBoolIndex() {
					arg1 = xs.CMD_PREFIX_BOOLEAN
				}
				cmd := xs.NewCommand(xs.CMD_QUERY_PREFIX, uint8(arg1), field.Vno, part1)
				s.buffer = append(s.buffer, cmd)
				s.queryPrefix[part1] = true
			}
		}
	}

	page, _ := xs.Pack("II", offset, size)
	cmd := xs.NewCommand(xs.CMD_SEARCH_GET_RESULT, 0, s.defaultOp, keyword, page)

	//发送指令
	var res *xs.Command
	var err error
	if len(s.buffer) > 0 {
		s.buffer = append(s.buffer, cmd)
		res, err = s.connection.ExecBatch(s.buffer)
		s.clearBuffer()
	} else {
		res, err = s.connection.Exec(cmd)
	}
	if err != nil {
		return nil, err
	}

	//找到的结果数量
	pack, err := xs.UnPack("Icount", res.Buf)
	if err != nil {
		return nil, err
	}
	result := &Result{
		Count:     pack["count"].(uint32),
		Documents: make([]*xs.Document, 0),
	}

	//获取每个回应
	var doc *xs.Document
	for {
		res, err = s.connection.GetResponse()
		if err != nil {
			fmt.Println(err)
			break
		}
		switch res.Cmd {
		case xs.CMD_SEARCH_RESULT_DOC:
			doc, err = xs.NewDocument(res.Buf)
			if err == nil {
				result.Documents = append(result.Documents, doc)
			}
		case xs.CMD_SEARCH_RESULT_FIELD:
			if doc != nil {
				if field, ok := s.setting.Schema.Fields[uint8(res.GetArg())]; ok {
					doc.Fields[field.Name] = res.Buf
				}
			}
		case xs.CMD_OK:
			goto ForEnd
		}
	}

ForEnd:

	//返回
	return result, nil
}

func (s *Searcher) Terms(keyword string) ([]string, error) {
	cmd := xs.NewCommand(xs.CMD_QUERY_GET_TERMS, 0, s.defaultOp, keyword)
	res, err := s.connection.Exec(cmd)
	if err != nil {
		return nil, err
	}
	return strings.Split(res.Buf, " "), nil
}

func (s *Searcher) GetExpandedQuery(keyword string, size uint8) ([]string, error) {

	if size <= 0 {
		size = 1
	}
	if size > 20 {
		size = 20
	}

	cmd := xs.NewCommand(xs.CMD_QUERY_GET_EXPANDED, size, 0, keyword)
	res, err := s.connection.Exec(cmd)
	if err != nil {
		return nil, err
	}

	//获取每个回应
	var result []string
	for {
		res, err = s.connection.GetResponse()
		if err != nil {
			fmt.Println(err)
			break
		}
		switch res.Cmd {
		case xs.CMD_SEARCH_RESULT_FIELD:
			result = append(result, res.Buf)
		case xs.CMD_OK:
			goto ForEnd
		}
	}

ForEnd:

	//返回
	return result, nil
}

func (s *Searcher) GetSynonyms(word string) []string {
	if word != "" {
		cmd := xs.NewCommand(xs.CMD_SEARCH_GET_SYNONYMS, 2, 0, word)
		res, err := s.connection.Exec(cmd)
		if err == nil && res.Buf != "" {
			return strings.Split(res.Buf, "\n")
		}
	}
	return []string{}
}

func (s *Searcher) GetAllSynonyms(stemmed bool) map[string][]string {
	arg1 := uint8(0)
	if stemmed {
		arg1 = 1
	}
	cmd := xs.NewCommand(xs.CMD_SEARCH_GET_SYNONYMS, arg1, 0)
	res, err := s.connection.Exec(cmd)

	words := make(map[string][]string)
	if err == nil && res.Buf != "" {
		for _, buf := range strings.Split(res.Buf, "\n") {
			sys := strings.Split(buf, "\t")
			words[sys[0]] = sys[1:]
		}
	}

	return words
}

func (s *Searcher) clearBuffer() {
	if s.buffer != nil && len(s.buffer) > 0 {
		s.buffer = append(s.buffer[len(s.buffer):])
	}
}
