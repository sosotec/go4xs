package go4xs

import (
	"gitee.com/sosotec/go4xs/xs"
	"log"
	"sync"
)

type Pool struct {
	m       sync.Mutex                                //保证多个goroutine访问时候,closed的线程安全
	res     chan *xs.Connection                       //连接存储的chan
	factory func(addr string) (*xs.Connection, error) //新建连接的工厂方法
	closed  bool                                      //连接池关闭标志
	addr    string
}

func NewPool(addr string, fn func(addr string) (*xs.Connection, error), size uint) *Pool {
	if size <= 0 {
		size = 5
	}
	return &Pool{
		addr:    addr,
		factory: fn,
		res:     make(chan *xs.Connection, size),
	}
}

func (p *Pool) Acquire() (*xs.Connection, error) {
	select {
	case r, ok := <-p.res:
		if !ok {
			return p.Acquire()
		}
		return r, nil
	default:
		return p.factory(p.addr)
	}
}

func (p *Pool) Close() {

	p.m.Lock()
	defer p.m.Unlock()

	if p.closed {
		return
	}
	p.closed = true

	//关闭通道，不让写入了
	close(p.res)

	//关闭通道里的资源
	for r := range p.res {
		r.Close()
	}
}

func (p *Pool) Release(r *xs.Connection) {

	//保证该操作和Close方法的操作是安全的
	p.m.Lock()
	defer p.m.Unlock()

	//资源池都关闭了，就省这一个没有释放的资源了，释放即可
	if p.closed {
		r.Close()
		return
	}

	select {
	case p.res <- r:
		log.Println("资源释放到池子里了")
	default:
		log.Println("资源池满了，释放这个资源吧")
		r.Close()
	}
}
