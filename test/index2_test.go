package test

import (
	"gitee.com/sosotec/go4xs"
	"gitee.com/sosotec/go4xs/xs"
	"testing"
)

func TestIndexer2GetScwsMulti(t *testing.T) {

	var setting = xs.NewSetting("../demo.ini")

	var indexer = &go4xs.Indexer{}
	defer indexer.Close()
	indexer.LoadConf(setting)
	if err := indexer.Connect(); err != nil {
		t.Fatal(err)
	}

	level, err := indexer.GetScwsMulti()
	if err != nil {
		t.Fatal(err)
	}

	t.Log(level)
}
