package test

import (
	"fmt"
	"gitee.com/sosotec/go4xs"
	"gitee.com/sosotec/go4xs/xs"
	"testing"
	"time"
)

func TestIndexerAdd1(t *testing.T) {

	var setting = xs.NewSetting("../xunsearch.ini")

	var indexer = &go4xs.Indexer{}
	defer indexer.Close()
	indexer.LoadConf(setting)
	if err := indexer.Connect(); err != nil {
		t.Fatal(err)
	}

	var data = map[string]string{
		"fid":     fmt.Sprintf("%v", time.Now().UnixNano()/1e6),
		"subject": "湖北随县柳林镇遭遇极端强降雨：已致21人遇难，4人失联",
		"content": "8月11日至12日，随县柳林镇发生极端强降雨天气。从11日21时至12日9时，柳林镇累计降雨503毫米，12日4时至7时降雨量达373.7毫米，5时、6时连续两个小时降雨量超过100毫米，均为有气象记录以来的历史极值。柳林镇镇区三面环山，平均积水深度达3.5米，最深处达5米。",
		"img":     "http://baijiahao.baidu.com/s?id=1707934367912244043",
		"tid":     "2",
		"time":    time.Now().Format("2006-01-02 15:04:05"),
	}
	//var data = map[string]string{
	//	"pid":     fmt.Sprintf("%v", time.Now().UnixNano()/1e6),
	//	"subject": "主题没有内容1",
	//	"message": "十六进制说明2 456",
	//	"chrono":  time.Now().Format("20060102150405"),
	//}
	//fmt.Println(data)

	err := indexer.Add(data)
	if err != nil {
		t.Fatal(err)
	}

	_ = indexer.FlushIndex()

	t.Log("ok")
}

func TestIndexerAdd2(t *testing.T) {

	var setting = xs.NewSetting("../xunsearch.ini")

	var indexer = &go4xs.Indexer{}
	defer indexer.Close()
	indexer.LoadConf(setting)
	if err := indexer.Connect(); err != nil {
		t.Fatal(err)
	}

	var data1 = map[string]string{
		"pid":     fmt.Sprintf("%v", time.Now().UnixNano()/1e6),
		"subject": "中国驻美大使就台湾问题明确向美方阐述中方立场",
		"content": "秦刚在会见结束后对记者表示，双方谈得很深入，也很坦诚，充分交换了意见。双方一致认为，中美双边关系非常重要，要通过对话沟通解决问题，管控好分歧和矛盾，改善两国关系。",
		"img":     "",
		"tid":     "2",
		"time":    time.Now().Format("2006-01-02 15:04:05"),
	}
	fmt.Println(data1)

	err1 := indexer.Add(data1)
	if err1 != nil {
		t.Fatal(err1)
	}

	var data2 = map[string]string{
		"pid":     fmt.Sprintf("%v", time.Now().UnixNano()/1e6),
		"subject": "香港中小学语文课新学年将增文言文教学，香港教育局希望2024年全面落实",
		"content": "【环球网报道 记者 康博浩】据香港“东网”消息，香港中小学生将加强语文课的文言文学习，今年9月的新学年起，小学、初中至高中的语文课程中将增设总共93篇经典文言文作品，不排除相关篇章会列入中学文凭试（“香港高考”）的指定考核作品范围，香港教育局期望全港学校在2024年的学年可以全面落实。",
		"img":     "",
		"tid":     "2",
		"time":    time.Now().Format("2006-01-02 15:04:05"),
	}
	fmt.Println(data2)

	err2 := indexer.Add(data2)
	if err2 != nil {
		t.Fatal(err2)
	}

	_ = indexer.FlushIndex()

	t.Log("ok")
}

func TestIndexerAdd21(t *testing.T) {

	var setting = xs.NewSetting("../xunsearch.ini")

	var indexer = &go4xs.Indexer{}
	defer indexer.Close()
	indexer.LoadConf(setting)
	if err := indexer.Connect(); err != nil {
		t.Fatal(err)
	}

	var data = map[string]string{
		"pid":     fmt.Sprintf("%v", time.Now().UnixNano()/1e6),
		"subject": "iPhone 12",
		"message": "iPhone 12",
		"chrono":  time.Now().Format("20060102150405"),
	}
	fmt.Println(data)

	err := indexer.Add(data)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("ok")
}

func TestIndexerAdd22(t *testing.T) {

	var setting = xs.NewSetting("../demo.ini")

	var indexer = &go4xs.Indexer{}
	defer indexer.Close()
	indexer.LoadConf(setting)
	if err := indexer.Connect(); err != nil {
		t.Fatal(err)
	}

	var data = map[string]string{
		"pid":     fmt.Sprintf("%v", time.Now().UnixNano()/1e6),
		"subject": "iPhone 12 Pro Max",
		"message": "iPhone 12",
		"chrono":  time.Now().Format("20060102150405"),
	}
	fmt.Println(data)

	err := indexer.Add(data)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("ok")
}

func TestIndexerAddSynonym11(t *testing.T) {

	var setting = xs.NewSetting("../demo.ini")

	var indexer = &go4xs.Indexer{}
	defer indexer.Close()
	indexer.LoadConf(setting)
	if err := indexer.Connect(); err != nil {
		t.Fatal(err)
	}

	err := indexer.AddSynonym("iPhone", "苹果")
	if err != nil {
		t.Fatal(err)
	}

	t.Log("ok")
}

func TestIndexerAddSynonym12(t *testing.T) {

	var setting = xs.NewSetting("../demo.ini")

	var indexer = &go4xs.Indexer{}
	defer indexer.Close()
	indexer.LoadConf(setting)
	if err := indexer.Connect(); err != nil {
		t.Fatal(err)
	}

	err := indexer.AddSynonym("苹果", "iPhone")
	if err != nil {
		t.Fatal(err)
	}

	t.Log("ok")
}

func TestIndexerDelete1(t *testing.T) {

	var setting = xs.NewSetting("../demo.ini")

	var indexer = &go4xs.Indexer{}
	defer indexer.Close()
	indexer.LoadConf(setting)
	if err := indexer.Connect(); err != nil {
		t.Fatal(err)
	}

	err := indexer.Delete("1627718163831")
	if err != nil {
		t.Fatal(err)
	}

	t.Log("ok")
}

func TestIndexerUpdate1(t *testing.T) {

	var setting = xs.NewSetting("../demo.ini")

	var indexer = &go4xs.Indexer{}
	defer indexer.Close()
	indexer.LoadConf(setting)
	if err := indexer.Connect(); err != nil {
		t.Fatal(err)
	}

	var data = map[string]string{
		"pid":     "1627719154471",
		"subject": "主题没有内容",
		"message": "十六进制说明十六进制说明十六进制说明十六进制说明十六进制说明十六进制说明",
		"chrono":  time.Now().Format("20060102150405"),
	}
	fmt.Println(data)

	err := indexer.Update(data)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("ok")
}
