package test

import (
	"encoding/json"
	"fmt"
	"gitee.com/sosotec/go4xs"
	"gitee.com/sosotec/go4xs/xs"
	"testing"
)

func TestSearcherQuery1(t *testing.T) {

	var setting = xs.NewSetting("../xunsearch.ini")
	var pool = go4xs.NewPool(setting.SearchServer, xs.NewConnection, 2)

	var searcher = &go4xs.Searcher{}
	defer searcher.Close()
	searcher.LoadConf(setting, pool)
	if err := searcher.Connect(); err != nil {
		t.Fatal(err)
	}
	searcher.SetFuzzy(true)

	res, err := searcher.Query("456")
	if err != nil {
		t.Fatal(err)
	}

	b, _ := json.Marshal(res.Documents)
	fmt.Printf("\ncount: %v \n%s\n\n", res.Count, string(b))

	t.Log(res)
}

func TestSearcherQuery2(t *testing.T) {

	var setting = xs.NewSetting("../demo.ini")
	var pool = go4xs.NewPool(setting.SearchServer, xs.NewConnection, 2)

	var searcher = &go4xs.Searcher{}
	defer searcher.Close()
	searcher.LoadConf(setting, pool)
	if err := searcher.Connect(); err != nil {
		t.Fatal(err)
	}

	res, err := searcher.Query("pid:8888")
	if err != nil {
		t.Fatal(err)
	}

	b, _ := json.Marshal(res.Documents)
	fmt.Printf("\ncount: %v \n%s\n\n", res.Count, string(b))

	t.Log(res)
}

func TestSearcherQuery3(t *testing.T) {

	var setting = xs.NewSetting("../demo.ini")
	var pool = go4xs.NewPool(setting.SearchServer, xs.NewConnection, 2)

	var searcher = &go4xs.Searcher{}
	defer searcher.Close()
	searcher.LoadConf(setting, pool)
	if err := searcher.Connect(); err != nil {
		t.Fatal(err)
	}

	res, err := searcher.Query("chrono:123456791")
	if err != nil {
		t.Fatal(err)
	}

	b, _ := json.Marshal(res.Documents)
	fmt.Printf("\ncount: %v \n%s\n\n", res.Count, string(b))

	t.Log(res)
}

func TestSearcherQuery41(t *testing.T) {

	var setting = xs.NewSetting("../demo.ini")
	var pool = go4xs.NewPool(setting.SearchServer, xs.NewConnection, 2)

	var searcher = &go4xs.Searcher{}
	defer searcher.Close()
	searcher.LoadConf(setting, pool)
	if err := searcher.Connect(); err != nil {
		t.Fatal(err)
	}

	res, err := searcher.Query("iphone")
	if err != nil {
		t.Fatal(err)
	}

	b, _ := json.Marshal(res.Documents)
	fmt.Printf("\ncount: %v \n%s\n\n", res.Count, string(b))

	t.Log(res)
}

func TestSearcherQuery42(t *testing.T) {

	var setting = xs.NewSetting("../demo.ini")
	var pool = go4xs.NewPool(setting.SearchServer, xs.NewConnection, 2)

	var searcher = &go4xs.Searcher{}
	defer searcher.Close()
	searcher.LoadConf(setting, pool)
	if err := searcher.Connect(); err != nil {
		t.Fatal(err)
	}

	searcher.SetAutoSynonyms()
	res, err := searcher.Query("苹果")
	if err != nil {
		t.Fatal(err)
	}

	b, _ := json.Marshal(res.Documents)
	fmt.Printf("\ncount: %v \n%s\n\n", res.Count, string(b))

	t.Log(res)
}

func TestSearcherTerms(t *testing.T) {

	var setting = xs.NewSetting("../demo.ini")
	var pool = go4xs.NewPool(setting.SearchServer, xs.NewConnection, 2)

	var searcher = &go4xs.Searcher{}
	defer searcher.Close()
	searcher.LoadConf(setting, pool)
	if err := searcher.Connect(); err != nil {
		t.Fatal(err)
	}

	res, err := searcher.Terms("中华人民共和国国歌")
	if err != nil {
		t.Fatal(err)
	}

	t.Log(res)
}

func TestSearcherGetExpandedQuery(t *testing.T) {

	var setting = xs.NewSetting("../demo.ini")
	var pool = go4xs.NewPool(setting.SearchServer, xs.NewConnection, 2)

	var searcher = &go4xs.Searcher{}
	defer searcher.Close()
	searcher.LoadConf(setting, pool)
	if err := searcher.Connect(); err != nil {
		t.Fatal(err)
	}
	res, err := searcher.GetExpandedQuery("cs", 5)
	if err != nil {
		t.Fatal(err)
	}

	b, _ := json.Marshal(res)
	fmt.Printf("\n%s\n\n", string(b))

	t.Log(res)
}

func TestSearcherGetAllSynonyms1(t *testing.T) {

	var setting = xs.NewSetting("../demo.ini")
	var pool = go4xs.NewPool(setting.SearchServer, xs.NewConnection, 2)

	var searcher = &go4xs.Searcher{}
	defer searcher.Close()
	searcher.LoadConf(setting, pool)
	if err := searcher.Connect(); err != nil {
		t.Fatal(err)
	}
	res := searcher.GetAllSynonyms(false)

	b, _ := json.Marshal(res)
	fmt.Printf("\n%s\n\n", string(b))

	t.Log(res)
}
